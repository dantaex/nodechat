

express = require('express');
router  = require('./server/router');
http 	= require('http');
fm 	 	= require('./server/utils/files');
path 	= require('path');   
db 		= require('./server/models');//Database
auth 	= require('./server/utils/auth');// auth
cookie  = require("cookie");
connect = require("connect");
store = new express.session.MemoryStore;

//Web server
var app = express();
app.configure(function(){
	app.set('port', process.env.PORT || 80);
	app.set('views', __dirname + '/server/views');
	app.set('view engine', 'jade');
	app.use(express.favicon());
	app.use(express.bodyParser());
	app.use(express.cookieParser('node_chat')); 
    app.use(express.session({secret: 'kjndkjasnkdjakdnwi98798', key: 'express.sid',store: store}));
	app.use(express.logger('dev'));
	app.use(express.methodOverride());
	app.use(app.router);		
	app.use(require('stylus').middleware(__dirname + '/public'));
	app.use(express.static(path.join(__dirname, 'public')));
});

//auth session (out of configure please)
app.use(auth.configureSession);

if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}

var server = http.createServer(app).listen(app.get('port'), function(){
	console.log('node_chat server listo en [' + app.get('port')+']');
});

//web routing
app.get('/',       router.index);
app.get('/login',  router.login);
app.get('/logout',  router.logout);
app.post('/login', router.postlogin);
app.get('/signup',  router.getsignup);
app.post('/signup', auth.userExist, router.postsignup);

var io = require('socket.io').listen(server);
io.set('authorization', function (handshakeData, accept) {

  if (handshakeData.headers.cookie) {
    handshakeData.cookie = cookie.parse(handshakeData.headers.cookie);
    handshakeData.sessionID = connect.utils.parseSignedCookie(handshakeData.cookie['express.sid'], 'kjndkjasnkdjakdnwi98798');
    if (handshakeData.cookie['express.sid'] == handshakeData.sessionID) {
    	return accept('Cookie is invalid.', false);
    }
  } 
  else{
    return accept('No cookie transmitted.', false);
  }
  accept(null, true);
});
io.sockets.on('connection', router.connection);
