
function render(req,res,view,options){
	res.render(view,options);
}

exports.index = function(req, res){
  if(req.session.user){
  	db.comments.findComs(function(err,comments){

  		if(err){
  			render(req,res,'webapp',
  				{
  					title:'Bienvenido(a)',
  					username:req.session.username,
  					errormessage : 'Algo pasó'
  				}
  			);
  		}
  		else{
			render(req,res,'webapp',
				{
					title    : 'Bienvenido(a)',
					username : req.session.username,
					comments : comments.reverse()
				}
			);  			
  		}
  	});
  } 
  else render(req,res,'login', {title: 'Hola' });
};

//statics -----------------------------------------------------------------------
exports.login = function(req,res){
	if(req.session.error) render(req,res,'login', {title: 'Login',errormessage : 'Inicia sesión para acceder'});
	else{
		render(req,res,'login', {title: 'Login'});
	}	
};
exports.logout = function (req, res) {
    req.session.destroy(function () {
        res.redirect('/');
    });
};

exports.postlogin = function(req,res){
	auth.login(req,res,function(err,sucess){
		if(!sucess) render(req,res,"login",{errormessage: 'No sé quien eres'});
		else res.redirect('/');
	});
};

exports.getsignup = function (req, res) {
  if (req.session.user) res.redirect("/");
  else                  render(req,res,"signup",{title: 'Registro'});
};
exports.postsignup = function(req, res){
	db.users.add(req,function(err,newuser){
		if(!newuser){
			render(req,res,'signup',{errormessage:'Auch!, Algo malo sucedió, intenta de nuevo'});
			console.log(err);
		} 
		else{
			//log him/her in!
			auth.startsession(newuser,req,function(err,sucess){
				if(sucess) res.redirect('/');//back to home baby!
				else render(req,res,'signup',{errormessage:'Ya eres miembro del club, pero hubo un problema al iniciar tu sesión =/'});
			});
		}
	});
}

exports.connection = function (socket) {
  //socket.emit('hello', { hello: 'Servidor conectado, :)' });
  //socket.on('hello', function(data){ console.log('client says:'+data.hello); });

 	parsed_id_arr = socket.handshake.cookie['express.sid'].match(/^s:(.*)\./);
    user_session_id = parsed_id_arr[1];

    if(store.sessions[user_session_id] == undefined){
    	console.log('User coming:');
    	console.log(socket.handshake.cookie);
    	console.log('Allowed users:');
    	console.log(store.sessions);
    	socket.emit('reload',{});
    }
    else{
    	socket.user_info = JSON.parse(store.sessions[user_session_id]);
    }  
  
  socket.on('addcomment', function (data) {
    console.log('About to add new comment:');
    console.log('from: '+socket.user_info.user.username)
    console.log('comment: '+data.text);

	db.comments.add({text:data.text,user:socket.user_info.user},function(err,addedcomment){
		if(err){ console.log('databse error:'); console.log(err); return;}
    	console.log('Comment saved successfully!');
    	socket.broadcast.emit('newcomment', addedcomment );
    	socket.emit('done', addedcomment );
	});

  });

}