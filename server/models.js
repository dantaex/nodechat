var mongoose = require('mongoose');
mongoose.connection.on("open", function(){console.log("Connected to MongoDB");});
mongoose.connect('mongodb://localhost:27017/nodechat');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
//encriptor
var hash = require('./utils/encript').hash;

var user = new Schema(
	{ 
		_id 	 : { type: String, required: true }, 
		username : { type: String, required: true, validate: /^([0-9a-zA-Z]){3,36}$/ },
		email 	 : { type: String, required: true, validate: /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/ },
		password : { type: String },
		salt 	 : { type: String },
		added 	 : { type: Date, 	default: Date.now }
	},
	{collection : 'users'}
);
//methods
user.statics.add = function (req,callback){
	var newuser = new this();

	if(req.files.pic && req.files.pic.size < 512000){
		var basicpath = path.join(__dirname,'/../public/pics/');
		var filename = path.join(basicpath,'pic_'+req.body.username+'.jpg');
		//create file
		fm.saveFile(req.files.pic.path, filename, function(err){ if(err) console.log(err); else console.log('User picture saved'); });
		newuser.pic = filename;
	}


	hash(req.body.password, function (err, salt, hash) {
		if (err){ callback('problem hashing password before insertion : '+err); return;}

		newuser._id 	 = req.body.username;
		newuser.username = req.body.username;
		newuser.password = hash;
		newuser.email = req.body.email;
		newuser.salt= salt;
		
		newuser.save(function(err,addeduser){
			if (err){callback('could not insert user : '+err); return;}
			auth.authenticate(req.body.username,req.body.password,function(err,loggeduser){
				if(err) console.log('error authenticating user : '+err);
				else	callback(null,loggeduser);
			});			
		});
	});
};
exports.users = mongoose.model('User',user);

var comment = new Schema(
	{ 
		user : { type: String,  required: true},
		text 	: { type: String,  required: true},
		added 	: { type: Number, 	 default: Date.now },
	},
	{collection : 'comments'}
);


comment.statics.findComs = function (callback){
	this.find({}, callback);
};

comment.statics.add = function(data,callback){
	var newcom      = new this();
	newcom.user 	= data.user.username;
	newcom.text		= data.text;

	newcom.save(function(err,addedcomment){
		if (err)callback(err);
		else callback(null,addedcomment);
	});
};
exports.comments = mongoose.model('Comment',comment);