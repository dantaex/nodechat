
/*
 * Authentication module
 */

var hash = require('./encript').hash;
var methods = {};

methods.authenticate = function(name, pass, callback) {
    if (!module.parent) console.log('Auth atempt [%s:%s]', name, pass);
    db.users.findOne({username: name},
		function (err, user) {			
			if (!user) return callback('cannot find user');
			else{
				hash(pass, user.salt, function (err, hash) {
					if (err) return callback(err);
					if (hash == user.password) return callback(null, user);
					callback('invalid password');
				});
			}
		}
	);
};
exports.authenticate = methods.authenticate;

methods.requiredAuthentication = function(req, res, next) {
    if (req.session.user)  next();
    else {
        req.session.error = 'Access denied';
        res.redirect('/login');
    }
};
exports.requiredAuthentication = methods.requiredAuthentication;

methods.userExist = function(req, res, next) {
    db.users.count({username: req.body.username}, 
    	function (err, count) {
			if (count === 0) next();
			else {
				req.session.error = "User Exist"
				res.render("signup",{errormessage:res.__('That username is already taken')});
			}
    	}
    );
};
exports.userExist = methods.userExist;

//configuration
methods.configureSession = function(req, res, next) {
	var err = req.session.error,
		msg = req.session.success;

	//delete old statuses
	delete req.session.error;
	delete req.session.success;

	res.locals.message = {};
	if (err) res.locals.message = {error: err};
	if (msg) res.locals.message = {sucess:msg};
	next();
};
exports.configureSession = methods.configureSession;

methods.startsession = function(user,req,callback){
	req.session.regenerate(function(){
		req.session.user 	 = user;
		req.session.username = user.username;
		req.session.success  = 'logged in as [username:' + user.username + '] since: '+(Date.now() / 1000);
		callback(null,true);
	});
};
exports.startsession = methods.startsession;

methods.login = function(req,res,callback){
	methods.authenticate(req.body.username,req.body.password,function(err,user){
		if(!user) callback(err);
		else{
			//start user's session
			methods.startsession(user,req,function(err,sucess){
				if(err) callback(err);
				else 	callback(null,true);
			});
		} 
	});
};
exports.login = methods.login;