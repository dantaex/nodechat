var fs = require('fs'),
	path = require('path'),
	mime = require('mime');

exports.fs = fs;

exports.exists = function(file,callback){
	fs.exists(file, function(exists) {
		callback(exists);
	});
};

exports.streamFile = function(filepath,response){

	var	stat     = fs.statSync(filepath),
	 	mmtype   = mime.lookup(filepath);   

	response.writeHead(200, {
	    'Content-Type'  :  mmtype,
	    'Content-Length':  stat.size
	});

	var readStream = fs.createReadStream(filepath);
	// We replaced all the event handlers with a simple call to readStream.pipe()
	console.log('streaming '+mmtype+':'+filepath);
	readStream.pipe(response);
};

/*
	CAUTION: this function assumes, that the parent directory
	of "to" param exists.
*/
exports.saveFile = function (from,to,callback){
	fs.readFile(from, function (err, data) {
		if(err) callback('Error reading file');
		else{
			fs.writeFile(to, data, function (err) {
				if(err) callback('Error saving file');
				else callback(null, to);
			});
		}		
	});
};